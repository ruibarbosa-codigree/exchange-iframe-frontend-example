import { Injectable } from "@angular/core";
import { ILoginResponse } from "../models/login";
import { TopbarService } from "../services/topbar.service";
import { environment } from "src/environments/environment";

@Injectable({
  providedIn: 'root',
})
export class IframeService {

  user: ILoginResponse
  frameBase: URL = new URL(environment.iframeUrl);
  lang = this.topbarService.selectedLang;
  public loggedInToken = '';

  constructor(private topbarService: TopbarService) {
     //The Exchange iframe supports the following url parameters on the iframe SRC attribute, to set or sync some information from the parent:
    //See our docs  https://gitlab.com/ruibarbosa-codigree/exchange-iframe-documentation 1.4. Exchange iframe SRC parameters
    this.frameBase.searchParams.append('lang', this.lang);
  }

}