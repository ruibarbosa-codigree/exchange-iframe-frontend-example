import { Component, ElementRef, HostListener, OnInit, ViewChild } from '@angular/core';
import { IframeService } from '../iframe.service';
import { LoginService } from 'src/app/services/login.service';
import { Subject, distinctUntilChanged, takeUntil } from 'rxjs';
import { EExchangeMessages, IPostMsgCheckOrigin } from 'src/app/models/ExchangeMessages';
import { BaseComponent } from 'src/app/shared/components/base/base.component';
import { HttpErrorResponse } from '@angular/common/http';
import { Location } from '@angular/common';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent extends BaseComponent implements OnInit {
  @ViewChild('frame') frame: ElementRef;
  exchangeRouteChange = new Subject<string>();
  constructor(
    public iframeService: IframeService,
    private loginService: LoginService,
    private location: Location
  ) {
    super();
  }

  ngOnInit(): void {
    this.loginService.isLogIn()
      .pipe(takeUntil(this.destroy$)).subscribe((value) => {
        if (value) {
          this.rescheduleRefreshToken();
        }
      });

    this.loginService.loggedUser$.pipe(
      takeUntil(this.destroy$),
    ).subscribe((user) => {
      if (user && !user?.iframeToken) {
        this.loginService.loginV2().subscribe({
          next: (value) => {
            let user = this.loginService.getUserFromLocalStorage();
            user.iframeToken = value.token;
            //See our docs  https://gitlab.com/ruibarbosa-codigree/exchange-iframe-documentation 1.4. Exchange iframe SRC parameters
            //Token is required parameter to authentication in our iframe
            this.iframeService.frameBase.searchParams.append('token', value.token);
            this.loginService.updateUser(user);
          },
          error: (e: HttpErrorResponse) => {
            //Handle with errors as needed
          }
        });
      }
    });

    this.exchangeRouteChange.pipe(
      takeUntil(this.destroy$),
      distinctUntilChanged()
    ).subscribe((href) => {
      if (href == '/') {
        this.location.replaceState(this.iframeService.frameBase.pathname);
      } else {
        this.location.replaceState(this.iframeService.frameBase.pathname + href);
      }
    });
    this.appendHrefParam();

  }

  @HostListener('window:message', ['$event'])
  getExchangeMessages(event: MessageEvent) {
    if (event.data) {
      switch (event.data.type) {
        case EExchangeMessages.APPREADY:
          this.sendPostMessageForDomainValidation();
          break;
        case EExchangeMessages.HEIGHTCHANGE:
          // Implement as needed 
          break;
        case EExchangeMessages.SESSIONEXPIRED:
          this.processUserLogout();
          break;
        case EExchangeMessages.ROUTECHANGE:
          this.exchangeRouteChange.next(event.data.href);
          break;
        default:
          break;
      }
    }
  }
  /**
   *See our docs  https://gitlab.com/ruibarbosa-codigree/exchange-iframe-documentation 
   * 1.3 Operator domain validation – via iframe postMessage
   * Required
   */
  private sendPostMessageForDomainValidation() {
    const msg: IPostMsgCheckOrigin = {
      type: 'checkOrigin'
    }
    this.frame.nativeElement.contentWindow.postMessage(msg, this.iframeService.frameBase.origin);
  }
  //See our docs  https://gitlab.com/ruibarbosa-codigree/exchange-iframe-documentation 1.4. Exchange iframe SRC parameters
  //Optional href Encoded uri component, for the deep link pretended to show in the iframe. Sport page, event page, market page. 
  private appendHrefParam() {
    const href = this.location.path().replace(/\//, '');
    if (href) {
      if (this.iframeService.frameBase.searchParams.has(href)) {
        this.iframeService.frameBase.searchParams.delete('href');
        this.iframeService.frameBase.searchParams.set('href', href);
      } else {
        this.iframeService.frameBase.searchParams.append('href', href);
      }
    }
  }

  private processUserLogout() {
    this.loginService.doLogoutUser();
    this.iframeService.frameBase.searchParams.delete('token');
  }

  private rescheduleRefreshToken() {
    const lastTimeCalled = this.loginService.getAuthTimestamp();
    if (lastTimeCalled == null || lastTimeCalled < (Date.now() - this.loginService.getRescheduleTimeToCallRefreshToken())) {
      this.loginService.refreshToken().subscribe({
        next: (userResponse) => {
          let user = this.loginService.getUserFromLocalStorage();
          user.token = userResponse.token;
          user.authTimestamp = Date.now();
          this.loginService.updateUser(user);
        },
        error: () => {
          this.processUserLogout();
        }
      })
    }
    this.loginService.callRefreshToken().pipe(
      takeUntil(this.destroy$))
      .subscribe({
        next: (userResponse) => {
          let user = this.loginService.getUserFromLocalStorage();
          user.token = userResponse.token;
          user.authTimestamp = Date.now();
          this.loginService.updateUser(user);
        },
        error: () => {
          this.processUserLogout();
        }
      });
  }


}
