export interface ILoginRequest {
  username: string;
  password: string;
}

export interface ILoginResponse {
  firstName: string;
  availableBalance: number;
  exposure: number;
  currency: string;
  token: string;
  iframeToken?: string;
  type: string;
  authTimestamp: number;
}

export interface ILoginV2Response {
  baseUrl: string;
  token: string;
}