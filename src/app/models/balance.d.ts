
interface IDepositDialog {
  user: IUser;
  mod: string;
}


export interface IBalanceRequest{
    type: string;
    amount: number;
}

export interface IBalanceResponse{
    availableBalance: number;
    exposure: number;
}