//See our docs  https://gitlab.com/ruibarbosa-codigree/exchange-iframe-documentation 1.4. Exchange iframe SRC parameters
export enum EExchangeMessages {
    APPREADY = 'appReady',
    ROUTECHANGE = 'routeChange',
    HEIGHTCHANGE = 'heightChange',
    SESSIONEXPIRED= 'sessionExpired'
}

export interface IPostMsgCheckOrigin {
    type: "checkOrigin";
}