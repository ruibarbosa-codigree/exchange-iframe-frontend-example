interface IUser {
    username: string;
    firstName: string;
    currency: string;
    cashBalance: string;
    token: string;
}