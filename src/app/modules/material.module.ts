import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';
import { MatDialogModule } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatFormFieldModule } from '@angular/material/form-field';
import { ReactiveFormsModule } from '@angular/forms';

const matModules = [
  MatMenuModule,
  MatIconModule,
  MatDialogModule,
  MatIconModule,
  MatInputModule,
  MatSnackBarModule,
  MatSidenavModule,
  MatFormFieldModule,
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    ...matModules,
  ],
  exports: [
    ReactiveFormsModule,
    ...matModules,
  ]
})
export class MaterialModule { }
