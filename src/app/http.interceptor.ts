import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { Inject, Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { LoginService } from "./services/login.service";

@Injectable()
export class ApiInterceptor implements HttpInterceptor {

  private token;
  constructor(
    @Inject('BASE_API_URL') private baseUrl: string,
    private loginService: LoginService,
  ) {

  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    this.token = this.loginService.getTokenFromLS();
    const configuredRequest: HttpRequest<any> = this.getConfiguredRequest(req);
    return next.handle(configuredRequest)
  }

  private getConfiguredRequest(request: HttpRequest<any>): HttpRequest<any> {
    return request.clone({
      headers: this.token ? request.headers.append('Authorization', `Bearer ${this.token}`) : request.headers,
      withCredentials: true,
      url: this.baseUrl + request.url,
    });
  }

}