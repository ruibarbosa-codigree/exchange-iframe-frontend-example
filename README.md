# Exchange Integration Sample Angular Application

This is a sample Angular application designed to showcase.
Please see Exchange iframe [documentation](https://gitlab.com/ruibarbosa-codigree/exchange-iframe-documentation)

The sample code is not intended to be used as-is in a production environment and carries no assurances or guarantees.

## Getting Started
This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 16.1.6.
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.
### Prerequisites

Before you begin, ensure you have the following installed:
- Angular (16)
- Node 18.10.0

### Usage

Follow these steps to use the sample code:

1. Clone the repository:
    ```bash
    git clone git@gitlab.com:ruibarbosa-codigree/exchange-iframe-frontend-example.git
    ```
   
2. Open the project in your preferred IDE (Vscode, webstorm, etc.).
3. Change environment.ts - put yours configurations.
4. Explore the code and its functionality.

## Support

For any questions or issues regarding this sample code, feel free to contact us.
